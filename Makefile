#Makefile
all: add-nbo

add-nbo: main.o
	g++ -o add-nbo main.o

main.o: main.cpp

clean:
	rm -f main.o
	rm -f add-nbo
