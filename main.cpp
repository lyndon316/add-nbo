#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <netinet/in.h>


int main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: %s file1 file2\n", argv[0]);
        return 1;
    }

    FILE* fp1 = fopen(argv[1], "rb");
    FILE* fp2 = fopen(argv[2], "rb");

    if (!fp1 || !fp2) {
        printf("no\n");
        return 1;
    }

    uint32_t a, b;

    fread(&a, sizeof(a), 1, fp1);
    fread(&b, sizeof(b), 1, fp2);

    a = ntohl(a);
    b = ntohl(b);

    printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n", a, a, b, b, a+b, a+b);

    fclose(fp1);
    fclose(fp2);

    return 0;
}
